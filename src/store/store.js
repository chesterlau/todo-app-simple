import Vue from 'vue';
import Vuex from 'vuex';
import TodoModule from '@/store/todo.module';

Vue.use(Vuex);

export const store = new Vuex.Store({
  modules: {
    TodoModule
  }
});
